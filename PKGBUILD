# Maintainer: Maxime Gauduin <alucryd@archlinux.org>
# Contributor: Ionut Biru <ibiru@archlinux.org>

pkgname=libva-vdpau-driver
pkgver=0.7.4
pkgrel=7
pkgdesc='VDPAU backend for VA API'
arch=(x86_64)
url=https://freedesktop.org/wiki/Software/vaapi
license=(GPL)
depends=(
  libgl
  libvdpau
  libx11
)
makedepends=(
  libva
  mesa
)
source=(
  https://freedesktop.org/software/vaapi/releases/libva-vdpau-driver/libva-vdpau-driver-${pkgver}.tar.bz2
  libva-vdpau-driver-0.7.4-glext-missing-definition.patch
  libva-vdpau-driver-0.7.4-libvdpau-0.8.patch
  libva-vdpau-driver-0.7.4-VAEncH264VUIBufferType.patch
  libva-vdpau-driver-0.7.4-CreateSurfaceFromV4L2Buf.patch
  libva-vdpau-driver-0.7.4-pthread.patch
  libva-vdpau-driver-0.7.4-vaGetDisplayDRM.patch
  libva-vdpau-driver-0.7.4-vaDestroyBuffer.patch
  sigfpe-crash.patch
  fallback-x.patch
  implement-vaquerysurfaceattributes.patch
  implement-vacreatesurfaces2.patch
  ffmpeg.patch
)
b2sums=('b9cd0bbbe1e638ad29363cd0d8c6452de222023017283ce81f138730c7ba3396f3ffca40478746cab4b93a8855e73de405aa783e44e6c1179c5e347bd7eff657'
        '3c295a68cb0976f976880f13296c703c3b75abdd3311b790c132ba233e9d26975686d8b618cdda594b1aa2e5cbadee850bf2e08cc35ca2c7ee11fe10c535d91c'
        'f1bc6e3840769ce4a5e53d85867a854ddfc780e670ca095541b9f2465ca0c96f3c7ed00da596f88d0c60aa749afcaf734670ada449c8a87e27f999c74539bc78'
        '5a5a12aff1f93769d480525140e7a0d61f4db64e57f1f956d778d1ab8be881b209779030ce14b8fee53dd4cbb6a7c59a0c9297ad0c92548268c840ea0f5910b0'
        '3a7daab7f6144837e50cabd06d30709bf7fc3218e226953471f2908efad2da78cb5035f9443642381460f6ffacd0eb0c9accf7e4084fcdf12e6942c6603dbb9a'
        'eab948d24435afa00ac4db7662a854c132afd7518619d56e0340440e16ac20fb5e9448864bb53b38c59da4abf7760c1ff44c77fa31ce2b1e06a79c2174220ec1'
        '30c675c7ccbed1ccdd522449d411707ee09e0e4908ddc50bb8f12bfcb32dac98b3526126c483cfd4b9283892d47cb6c166cd843f209bc658bcfc4c76de838359'
        '2570d9b2de19a69db46dccafa0714b0a3266c13222303b3cb1b38cda09b0924a9b3eda91ad9ba36c097423016581934d937e7e97719956406c4c8a54f674d76c'
        'c1e3a3af09c0c121747b0da28cb256ab4f9783254349be6accad512b7c4872065909bb6701ff0bee07c04e8c7ed34b93a7f6e50b964a1207501209d75f70d26b'
        'a15d6ce57c1415481d2763b4be799502bf03fbadfbf68433ba4994aee849d8b1b0d3b02ebb485ad4283443205ebb0e3ad6bd261f47591427710185114a261966'
        'bf4bebbfab6eb736daa90166cd2c8de50235a74d916b961a796553724edbbe2a136a746580f138c2bb284e6c8ae2c4861424f951bc85597f1080840aef539f7c'
        '97330f3d1678e14a804eef007d213fb4a50bc79af89613e2f86d6118eb9832a0d43e6d2f26f3530822d7d9c2e2704232f1a854cae704478eea4c27124412f518'
        'f628f316ea0460f8ce8ce7b787de402ca8fe85bec5612d1da6e0f5672cd92c0f660ab7d84968616c93dbde4db178f896259e302b26d1f6c8a461351164400c3b')

prepare() {
  cd libva-vdpau-driver-${pkgver}

  patch -Np1 -i ../libva-vdpau-driver-0.7.4-glext-missing-definition.patch
  patch -Np1 -i ../libva-vdpau-driver-0.7.4-libvdpau-0.8.patch
  patch -Np1 -i ../libva-vdpau-driver-0.7.4-VAEncH264VUIBufferType.patch
  patch -Np1 -i ../sigfpe-crash.patch
  patch -Np1 -i ../fallback-x.patch
  patch -Np1 -i ../implement-vaquerysurfaceattributes.patch
  patch -Np1 -i ../implement-vacreatesurfaces2.patch
  sed 's|Baseline|ConstrainedBaseline|g' -i src/vdpau_decode.c
  patch -Np1 -i ../libva-vdpau-driver-0.7.4-CreateSurfaceFromV4L2Buf.patch
  patch -Np1 -i ../libva-vdpau-driver-0.7.4-pthread.patch
  patch -Np1 -i ../libva-vdpau-driver-0.7.4-vaGetDisplayDRM.patch
  patch -Np1 -i ../libva-vdpau-driver-0.7.4-vaDestroyBuffer.patch
  patch -Np1 -i ../ffmpeg.patch
}

build() {
  cd libva-vdpau-driver-${pkgver}

  ./configure \
    --prefix=/usr
  make
}

package() {
  cd libva-vdpau-driver-${pkgver}

  make DESTDIR="${pkgdir}" install
}

# vim:set ts=2 sw=2 et:
